from rest_framework import serializers

class OtherFee(serializers.Serializer):
    type = serializers.CharField(required=False, default='')
    amount = serializers.CharField(required=False, default='')

class LoanTerm(serializers.Serializer):
    term = serializers.CharField(required=False, default='')
    amount_per_installment = serializers.CharField(required=False, default='')
    total = serializers.CharField(required=False, default='')

