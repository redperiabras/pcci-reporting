# -*- coding: utf-8 -*-
# @Time    : 2/5/21 10:16 AM
# @Author  : Red Periabras
# @File    : mortgage_declaration.py
# @Software: PyCharm

from rest_framework import serializers

from reports.models import Dummy

class VehicleInformation(serializers.Serializer):
    motor_no = serializers.CharField(required=False, default='')
    serial_no = serializers.CharField(required=False, default='')
    plate_no = serializers.CharField(required=False, default='')
    make = serializers.CharField(required=False, default='')

class MortgageDeclarationSerializer(serializers.ModelSerializer):
    class Meta:
        ref_name = 'cm_mortage_declaration'
        model = Dummy
        fields = '__all__'

    date = serializers.DateField(required=False, default=None)

    borrower_name = serializers.CharField(required=False, default='')
    principal_office = serializers.CharField(required=False, default='')

    city = serializers.CharField(required=False, default='')
    province = serializers.CharField(required=False, default='')

    properties = serializers.ListField(child=VehicleInformation(), required=False, default=[])
