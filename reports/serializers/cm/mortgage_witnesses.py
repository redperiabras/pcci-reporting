# -*- coding: utf-8 -*-
# @Time    : 2/5/21 9:07 PM
# @Author  : Red Periabras
# @File    : mortgage_witnesses.py
# @Software: PyCharm

from rest_framework import serializers

from reports.models import Dummy


class MortgageWitnessesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dummy
        fields = '__all__'

    date = serializers.DateField(required=False, default=None)
    borrower_name = serializers.CharField(required=False, default='')

    amount_in_words = serializers.CharField(required=False, default='')
    amount_in_figures = serializers.CharField(required=False, default='')

    pcci_tin = serializers.CharField(required=False, default='')
    pcci_city = serializers.CharField(required=False, default='')

    witness_1 = serializers.CharField(required=False, default='')
    witness_2 = serializers.CharField(required=False, default='')