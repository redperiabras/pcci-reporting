# -*- coding: utf-8 -*-
# @Time    : 2/5/21 10:06 PM
# @Author  : Red Periabras
# @File    : mortgage_acknowledgment.py
# @Software: PyCharmint

from rest_framework import serializers

from reports.models import Dummy

class PresentedIDSerializer(serializers.Serializer):
    name = serializers.CharField(required=False, default='')
    government_id = serializers.CharField(required=False, default='')
    expiry_date = serializers.CharField(required=False, default='')
    place_of_issue = serializers.CharField(required=False, default='')

class MortgageAcknowledgmentSerializer(serializers.ModelSerializer):
    class Meta:
        ref_name = 'cm_mortgage_acknowledgment'
        model = Dummy
        fields = '__all__'

    date = serializers.DateField(required=False, default=None)

    reserve_field = serializers.CharField(required=False, default=None)

    borrower_name = serializers.CharField(required=False, default='')
    borrower_address = serializers.CharField(required=False, default='')
    mortgagee_name = serializers.CharField(required=False, default='')

    notary_city = serializers.CharField(required=False, default='')

    pcci_id = PresentedIDSerializer(required=False, default={})

    presented_ids = serializers.ListField(child=PresentedIDSerializer(), required=False, default=[], max_length=4)

