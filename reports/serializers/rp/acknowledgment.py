# -*- coding: utf-8 -*-
# @Time    : 1/29/21 3:30 PM
# @Author  : Red Periabras
# @Software: PyCharm


from rest_framework import serializers

from reports.models import Dummy

class AcknowledgmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dummy
        fields = '__all__'

    borrower_signatory = serializers.CharField(required=False, default='')
    borrower_position = serializers.CharField(required=False, default='')
    borrower_id_no = serializers.CharField(required=False, default='')
    borrower_id_place_issue = serializers.CharField(required=False, default='')
    borrower_id_date_issue = serializers.CharField(required=False, default='')

    pcci_signatory = serializers.CharField(required=False, default='')
    pcci_position = serializers.CharField(required=False, default='')
    pcci_address = serializers.CharField(required=False, default='')
    pcci_id_no = serializers.CharField(required=False, default='')
    pcci_id_place_issue = serializers.CharField(required=False, default='')
    pcci_id_date_issue = serializers.CharField(required=False, default='')




