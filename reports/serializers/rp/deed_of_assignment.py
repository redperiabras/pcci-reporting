# -*- coding: utf-8 -*-
# @Time    : 2/6/21 3:07 PM
# @Author  : Red Periabras
# @File    : deed_of_assignment.py
# @Software: PyCharm

from rest_framework import serializers
from reports.models import Dummy

class RightsAndInterestsSerializer(serializers.Serializer):

    maturity_value = serializers.CharField(required=False, default='')
    value_date = serializers.CharField(required=False, default='')
    maturity_date = serializers.CharField(required=False, default='')
    drawer_bank = serializers.CharField(required=False, default='')
    check_number = serializers.CharField(required=False, default='')
    check_issuer = serializers.CharField(required=False, default='')

class DeedOfAssignmentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Dummy
        fields = '__all__'

    date = serializers.CharField(required=False, default='')

    maturity_value_in_words = serializers.CharField(required=False, default='')
    maturity_value_in_figures = serializers.CharField(required=False, default='')

    borrower_name = serializers.CharField(required=False, default='')
    borrower_position = serializers.CharField(required=False, default='')
    borrower_address = serializers.CharField(required=False, default='')
    borrower_signatory = serializers.CharField(required=False, default='')
    borrower_signatory_address = serializers.CharField(required=False, default='')

    by_virtue_of_the_laws_of = serializers.CharField(required=False, default='')

    pcci_name = serializers.CharField(required=False, default='')
    pcci_address = serializers.CharField(required=False, default='')

    rights_and_interests = serializers.ListField(child=RightsAndInterestsSerializer(), required=False, default=[])

    maturity_value = serializers.CharField(required=False, default='')

    witness_1 = serializers.CharField(required=False, default='')
    witness_2 = serializers.CharField(required=False, default='')

