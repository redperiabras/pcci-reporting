# -*- coding: utf-8 -*-
# @Time    : 1/29/21 3:29 PM
# @Author  : Red Periabras
# @File    : official_receipt.py.py
# @Software: PyCharm

from rest_framework import serializers

from ..models import Dummy


class PaymentParticulars(serializers.Serializer):
    particular = serializers.CharField(required=False, default='')
    acct_code = serializers.CharField(required=False, default='')
    amount = serializers.CharField(required=False, default='')


class CheckDetails(serializers.Serializer):
    check_number = serializers.CharField(required=False, default='')
    amount = serializers.CharField(required=False, default='')


class OfficialReceiptSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dummy
        fields = '__all__'

    or_number = serializers.CharField(required=False, default='')
    date = serializers.DateField(required=False)
    customer_name = serializers.CharField(max_length=45, required=False)
    tin = serializers.CharField(max_length=45, required=False, default='')
    business_style = serializers.CharField(max_length=45, required=False, default='')
    address = serializers.CharField(max_length=80, required=False, default='')
    amount_in_words = serializers.CharField(max_length=240, required=False)
    amount_in_figures = serializers.CharField(default='', required=False)
    account_number = serializers.CharField(default='', required=False)
    description = serializers.CharField(required=False, default='')

    payment_particulars = serializers.ListField(child=PaymentParticulars(), max_length=6, required=False)
    cash_payment = serializers.CharField(default='', required=False)
    check_payment = serializers.ListField(child=CheckDetails(), max_length=5, required=False)

    pcci_signatory = serializers.CharField(required=False, default='')