# -*- coding: utf-8 -*-
# @Time    : 2/6/21 10:41 PM
# @Author  : Red Periabras
# @File    : mortgage_acknowledgment.py
# @Software: PyCharm

from rest_framework import serializers

from reports.models import Dummy

class MortgageAcknowledgmentSerializer(serializers.ModelSerializer):
    class Meta:
        ref_name = 'rem_mortage_acknowledgment'
        model = Dummy
        fields = '__all__'

    date = serializers.DateField(required=False, default=None, help_text="Booking Date")

    pcci_name = serializers.CharField(required=False, default='')
    pcci_id_no = serializers.CharField(required=False, default='')
    pcci_id_place_of_issue = serializers.CharField(required=False, default='')
    pcci_id_date_of_issue = serializers.CharField(required=False, default='')

    borrower_name = serializers.CharField(required=False, default='')
    borrower_id_no = serializers.CharField(required=False, default='')
    borrower_id_place_of_issue = serializers.CharField(required=False, default='')
    borrower_id_date_of_issue = serializers.CharField(required=False, default='')

    spouse_name = serializers.CharField(required=False, default='')
    spouse_id_no = serializers.CharField(required=False, default='')
    spouse_id_place_of_issue = serializers.CharField(required=False, default='')
    spouse_id_date_of_issue = serializers.CharField(required=False, default='')

    rem_size = serializers.CharField(required=False, default='')

    witness_1 = serializers.CharField(required=False, default='')
    witness_2 = serializers.CharField(required=False, default='')

    notary_province_city = serializers.CharField(required=False, default='')


