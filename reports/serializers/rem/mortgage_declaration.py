# -*- coding: utf-8 -*-
# @Time    : 2/6/21 10:40 PM
# @Author  : Red Periabras
# @File    : mortgage_declaration.py
# @Software: PyCharm

from rest_framework import serializers

from reports.models import Dummy


class MortgageDeclarationSerializer(serializers.ModelSerializer):
    class Meta:
        ref_name = 'rem_mortage_declaration'
        model = Dummy
        fields = '__all__'

    date = serializers.DateField(required=False, default=None, help_text="Booking Date")
    pcci_address = serializers.CharField(required=False, default='')
    pcci_province = serializers.CharField(required=False, default='')

    borrower_name = serializers.CharField(required=False, default='')
    spouse_name = serializers.CharField(required=False, default='')
