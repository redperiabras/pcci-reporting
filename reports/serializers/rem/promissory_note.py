# -*- coding: utf-8 -*-
# @Time    : 1/29/21 3:30 PM
# @Author  : Red Periabras
# @Software: PyCharm


from rest_framework import serializers

from reports.models import Dummy

class PromissoryNoteSerializer(serializers.ModelSerializer):
    class Meta:
        ref_name='rem_promissory_note'
        model = Dummy
        fields = '__all__'

    date = serializers.DateField(required=False, default=None)

    amount_in_figures = serializers.CharField(required=False, default='')
    amount_in_words = serializers.CharField(required=False, default='')

    loanterm_in_figures = serializers.CharField(required=False, default='')
    loanterm_in_words = serializers.CharField(required=False, default='')

    monthlyamort_in_words = serializers.CharField(required=False, default='')
    monthlyamort_in_figures = serializers.CharField(required=False, default='')

    late_pay_percent = serializers.CharField(required=False, default='')

    start_date = serializers.DateField(required=False, default=None)
    maturity_date = serializers.DateField(required=False, default=None)

    interest_in_words = serializers.CharField(required=False, default='')
    interest_in_figures = serializers.CharField(required=False, default='')

    proceeds_receiver = serializers.CharField(required=False, default='')

    loan_purpose = serializers.CharField(required=False, default='')

    witness_1 = serializers.CharField(required=False, default='')
    witness_2 = serializers.CharField(required=False, default='')
    witness_3 = serializers.CharField(required=False, default='')
    witness_4 = serializers.CharField(required=False, default='')
