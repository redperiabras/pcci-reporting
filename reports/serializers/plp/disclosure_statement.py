# -*- coding: utf-8 -*-
# @Time    : 1/29/21 6:28 PM
# @Author  : Red Periabras
# @File    : disclosure_statement.py.py
# @Software: PyCharm

from rest_framework import serializers

from reports.models import Dummy
from ..helpers import OtherFee, LoanTerm

class DisclosureStatementSerializer(serializers.ModelSerializer):
    class Meta:
        ref_name = 'plp_disclosure_statement'
        model = Dummy
        fields = '__all__'

    date = serializers.DateField(required=False, default=None, help_text="Booking Date")
    borrower_name = serializers.CharField(required=False, default='')
    borrower_tin = serializers.CharField(required=False, default='')

    address = serializers.CharField(required=False, default='')
    net_in_words = serializers.CharField(required=False, default='')
    net_in_figures = serializers.CharField(required=False, default='')

    downpayment = serializers.CharField(required=False, default='')

    net_service_fee = serializers.CharField(required=False, default='')
    net_processing_fee = serializers.CharField(required=False, default='')
    net_share_capital = serializers.CharField(required=False, default='')
    net_membership_fee = serializers.CharField(required=False, default='')
    net_documentary_fee = serializers.CharField(required=False, default='')
    net_notarial_fee = serializers.CharField(required=False, default='')
    net_others = serializers.ListField(child=OtherFee(), max_length=2, required=False, default=[])
    net_total_non_finance = serializers.CharField(required=False, default='')

    net_to_be_financed = serializers.CharField(required=False, default='')

    charge_interest = serializers.CharField(required=False, default='')

    charge_start_date = serializers.CharField(required=False, default='')
    charge_due_date = serializers.CharField(required=False, default='')

    charge_total_interest = serializers.CharField(required=False, default='')
    charge_is_compound = serializers.BooleanField(required=False, default=False)
    charge_is_simple = serializers.BooleanField(required=False, default=False)
    charge_is_semi_monthly = serializers.BooleanField(required=False, default=False)
    charge_is_weekly = serializers.BooleanField(required=False, default=False)
    charge_is_quarterly = serializers.BooleanField(required=False, default=False)
    charge_is_monthly = serializers.BooleanField(required=False, default=False)
    charge_is_semi_annual = serializers.BooleanField(required=False, default=False)
    charge_is_annual = serializers.BooleanField(required=False, default=False)

    percentage_charge_to_total_net = serializers.CharField(required=False, default='')

    effective_interest_rate = serializers.CharField(required=False, default='')

    single_payment_due_date = serializers.DateField(required=False, default=None)
    single_payment_amount = serializers.CharField(required=False, default='')

    installment_monthly = serializers.ListField(child=LoanTerm(), max_length=2, required=False, default=[])
    installment_total = serializers.CharField(required=False, default='')

    pcci_tin = serializers.CharField(required=False, default='')
    pcci_signatory = serializers.CharField(required=False, default='')
    pcci_signatory_position = serializers.CharField(required=False, default='')
