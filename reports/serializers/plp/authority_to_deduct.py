# -*- coding: utf-8 -*-
# @Time    : 1/29/21 6:28 PM
# @Author  : Red Periabras
# @File    : disclosure_statement.py.py
# @Software: PyCharm

from rest_framework import serializers

from reports.models import Dummy


class AuthorityToDeductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dummy
        fields = '__all__'

    monthly_amort = serializers.CharField(required=False, default='')
    source_amort = serializers.CharField(required=False, default='')

    agent_name = serializers.CharField(required=False, default='')

    loan_term = serializers.CharField(required=False, default='')

    start_date = serializers.CharField(required=False, default='')

    loan_amount_in_words = serializers.CharField(required=False, default='')
    loan_amount_in_figures = serializers.CharField(required=False, default='')

    purchase_description = serializers.CharField(required=False, default='')

    borrower_name = serializers.CharField(required=False, default='')

    invoice_no = serializers.CharField(required=False, default='')
    invoice_date = serializers.CharField(required=False, default='')

    authorized_to_pay_directly = serializers.CharField(required=False, default='')

    borrower_payroll_no = serializers.CharField(required=False, default='')
    borrower_pay_grade = serializers.CharField(required=False, default='')