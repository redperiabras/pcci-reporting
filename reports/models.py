# -*- coding: utf-8 -*-
# @Time    : 1/28/21 5:28 PM
# @Author  : Red Periabras
# @Email   : redperiabras@gmail.com
# @File    : models.py.py
# @Software: PyCharm

from django_fake_model import models as f
from django.db import models


class Dummy(f.FakeModel):
    pass
