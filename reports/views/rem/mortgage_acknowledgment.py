# -*- coding: utf-8 -*-
# @Time    : 2/6/21 11:47 PM
# @Author  : Red Periabras
# @File    : mortgage_acknowledgment.py
# @Software: PyCharm


from reports.serializers.rem.mortgage_acknowledgment import MortgageAcknowledgmentSerializer

from io import BytesIO

from reportlab.pdfgen import canvas

from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont

from reportlab.lib.units import cm
from reportlab.lib.pagesizes import legal

from rest_framework import viewsets

from django.http import FileResponse

import textwrap

FONT_SIZE = 8

pdfmetrics.registerFont(TTFont('F25_Bank_Printer', './reports/assets/fonts/F25_Bank_Printer.ttf'))


class REMMortgageAcknowledgmentView(viewsets.ModelViewSet):
    serializer_class = MortgageAcknowledgmentSerializer
    http_method_names = ['post']

    def create(self, request):
        data = self.serializer_class(data=request.data)

        if not data.is_valid(raise_exception=True):
            return

        buffer = BytesIO()
        c = canvas.Canvas(buffer, pagesize=legal)

        c.setFont('F25_Bank_Printer', size=FONT_SIZE)
        c.setStrokeColorRGB(169, 169, 169)

        pcci_name = data.validated_data['pcci_name']
        c.drawString(2.6 * cm, (22.5 + 4.98) * cm, pcci_name)

        borrower_name = data.validated_data['borrower_name']
        c.drawString(11.6 * cm, (22.5 + 4.98) * cm, borrower_name)

        witness_1 = data.validated_data['witness_1']
        c.drawString(2.6 * cm, (20 + 4.98) * cm, witness_1)

        witness_2 = data.validated_data['witness_2']
        c.drawString(2.6 * cm, (19.15 + 4.98) * cm, witness_2)

        notary_province_city = data.validated_data['notary_province_city']
        c.drawString(10.8 * cm, (15.7 + 4.98) * cm, notary_province_city)

        date = data.validated_data['date']
        if date is not None:
            c.drawRightString(5.4 * cm, (15.4 + 4.98) * cm, "%d%s" % (date.day, "tsnrhtdd"[(date.day // 10 % 10 != 1) * (date.day % 10 < 4) * date.day % 10::4]))
            c.drawString(7.6 * cm, (15.4 + 4.98) * cm, date.strftime('%B, %y'))

        c.drawString(2.6 * cm, (13.4 + 4.98) * cm, pcci_name)

        pcci_id_no = data.validated_data['pcci_id_no']
        c.drawString(9.5 * cm, (13.4 + 4.98) * cm, pcci_id_no)

        pcci_id_place_of_issue = data.validated_data['pcci_id_place_of_issue']
        pcci_id_date_of_issue = data.validated_data['pcci_id_date_of_issue']
        c.drawString(13.1 * cm, (13.4 + 4.98) * cm, f'{pcci_id_place_of_issue} / {pcci_id_date_of_issue}')

        c.drawString(2.6 * cm, (12.75 + 4.98) * cm, borrower_name)

        borrower_id_no = data.validated_data['borrower_id_no']
        c.drawString(9.5 * cm, (12.75 + 4.98) * cm, borrower_id_no)

        borrower_id_place_of_issue = data.validated_data['borrower_id_place_of_issue']
        borrower_id_date_of_issue = data.validated_data['borrower_id_date_of_issue']
        c.drawString(13.1 * cm, (12.75 + 4.98) * cm, f'{borrower_id_place_of_issue} / {borrower_id_date_of_issue}')

        spouse_name = data.validated_data['spouse_name']
        c.drawString(2.6 * cm, (12 + 4.98) * cm, spouse_name)

        spouse_id_no = data.validated_data['spouse_id_no']
        c.drawString(9.5 * cm, (12 + 4.98) * cm, spouse_id_no)

        spouse_id_place_of_issue = data.validated_data['spouse_id_place_of_issue']
        spouse_id_date_of_issue = data.validated_data['spouse_id_date_of_issue']
        c.drawString(13.1 * cm, (12 + 4.98) * cm, f'{spouse_id_place_of_issue} / {spouse_id_date_of_issue}')

        rem_size = data.validated_data['rem_size']
        c.drawString(7.4 * cm, (7.6 + 4.98) * cm, rem_size)

        c.setTitle('REM - Mortgage Acknowldgment')
        c.showPage()
        c.save()

        buffer.seek(0)
        return FileResponse(buffer, as_attachment=True, filename=f'REM - Mortgage Acknowldgment.pdf')
