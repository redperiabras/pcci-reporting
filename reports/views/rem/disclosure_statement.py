# -*- coding: utf-8 -*-
# @Time    : 1/29/21 7:14 PM
# @Author  : Red Periabras
# @File    : disclosure_statement.py
# @Software: PyCharm

from reports.serializers.rem.disclosure_statement import DisclosureStatementSerializer

from io import BytesIO

from reportlab.pdfgen import canvas

from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont

from reportlab.lib.units import cm
from reportlab.lib.pagesizes import legal

from rest_framework import viewsets

from django.http import FileResponse

FONT_SIZE = 7

pdfmetrics.registerFont(TTFont('F25_Bank_Printer', './reports/assets/fonts/F25_Bank_Printer.ttf'))


class REMDisclosureStatementView(viewsets.ModelViewSet):
    serializer_class = DisclosureStatementSerializer
    http_method_names = ['post']

    def create(self, request):
        data = self.serializer_class(data=request.data)

        if not data.is_valid(raise_exception=True):
            return

        buffer = BytesIO()
        c = canvas.Canvas(buffer, pagesize=legal)

        c.setFont('F25_Bank_Printer', size=FONT_SIZE)
        c.setStrokeColorRGB(169, 169, 169)

        date = data.validated_data.get('date', None)

        borrower_name = data.validated_data.get('borrower_name', '')
        c.drawString(4.95 * cm, (28.55 + 2.55) * cm, borrower_name.upper())

        address = data.validated_data.get('address', '')
        c.drawString(3.2 * cm, (28.1 + 2.55) * cm, address)

        net_in_words = data.validated_data.get('net_in_words', '')
        c.drawString(5.0 * cm, (27.3 + 2.55) * cm, net_in_words.upper())

        net_in_figures = data.validated_data.get('net_in_figures', '')
        c.drawRightString(20.0 * cm, (27.3 + 2.55) * cm, net_in_figures)

        downpayment = data.validated_data.get('downpayment', '')
        c.drawRightString(20.0 * cm, (26.5 + 2.55) * cm, downpayment)

        unpaid_balance = data.validated_data.get('unpaid_balance', '')
        c.drawRightString(20.0 * cm, (26.0 + 2.55) * cm, unpaid_balance)

        net_processing_fee = data.validated_data.get('net_processing_fee')
        c.drawRightString(15.4 * cm, (25.2 + 2.55) * cm, net_processing_fee)

        net_appraisal_fee = data.validated_data.get('net_appraisal_fee')
        c.drawRightString(15.4 * cm, (24.8 + 2.55) * cm, net_appraisal_fee)

        net_registration_fee = data.validated_data.get('net_registration_fee')
        c.drawRightString(15.4 * cm, (24.4 + 2.55) * cm, net_registration_fee)

        net_documentary_fee = data.validated_data.get('net_documentary_fee', '')
        c.drawRightString(15.4 * cm, (24 + 2.55) * cm, net_documentary_fee)

        net_notarial_fee = data.validated_data.get('net_notarial_fee', '')
        c.drawRightString(15.4 * cm, (23.6 + 2.55) * cm, net_notarial_fee)

        net_service_fee = data.validated_data.get('net_service_fee', '')
        c.drawRightString(15.4 * cm, (23.2 + 2.55) * cm, net_service_fee)

        net_share_capital = data.validated_data.get('net_share_capital', '')
        c.drawRightString(15.4 * cm, (22.75 + 2.55) * cm, net_share_capital)

        init_y, delta_y = (22.3 + 2.55), 0.4
        for index, item in enumerate(data.validated_data.get('net_others', '')):
            y = init_y - index * delta_y
            c.drawString(3.8 * cm, y * cm, item['type'])
            c.drawRightString(15.4 * cm, y * cm, item["amount"])

        net_total_non_finance = data.validated_data.get('net_total_non_finance', '')
        c.drawRightString(20 * cm, (21.5 + 2.55) * cm, net_total_non_finance)

        net_to_be_financed = data.validated_data.get('net_to_be_financed', '')
        c.drawRightString(20 * cm, (21 + 2.55) * cm, net_to_be_financed)

        charge_interest = data.validated_data.get('charge_interest', '')
        c.drawRightString(4.9 * cm, (20.2 + 2.55) * cm, charge_interest)

        charge_start_date = data.validated_data.get('charge_start_date', '')
        c.drawString(6.9 * cm, (20.2 + 2.45) * cm, charge_start_date)

        charge_due_date = data.validated_data.get('charge_due_date', '')
        c.drawString(9.5 * cm, (20.2 + 2.45) * cm, charge_due_date)

        charge_total_interest = data.validated_data.get('charge_total_interest', '')
        c.drawRightString(20 * cm, (20.2 + 2.55) * cm, charge_total_interest)

        if data.validated_data['charge_is_compound']:
            c.drawString(2.8 * cm, (19.8 + 2.55) * cm, "/")

        if data.validated_data['charge_is_simple']:
            c.drawString(2.8 * cm, (19.3 + 2.55) * cm, "/")

        if data.validated_data['charge_is_semi_monthly']:
            c.drawString(5.1 * cm, (19.8 + 2.55) * cm, "/")

        if data.validated_data['charge_is_weekly']:
            c.drawString(5.1 * cm, (19.3 + 2.55) * cm, "/")

        if data.validated_data['charge_is_quarterly']:
            c.drawString(7.8 * cm, (19.8 + 2.55) * cm, "/")

        if data.validated_data['charge_is_monthly']:
            c.drawString(7.8 * cm, (19.3 + 2.55) * cm, "/")

        if data.validated_data['charge_is_semi_annual']:
            c.drawString(10.1 * cm, (19.8 + 2.55) * cm, "/")

        if data.validated_data['charge_is_annual']:
            c.drawString(10.1 * cm, (19.3 + 2.55) * cm, "/")

        percentage_charge_to_total_net = data.validated_data.get('percentage_charge_to_total_net', '')
        c.drawRightString(12.8 * cm, (18.9 + 2.55) * cm, percentage_charge_to_total_net)

        effective_interest_rate = data.validated_data.get('effective_interest_rate', '')
        c.drawRightString(11.2 * cm, (17.1 + 2.55) * cm, effective_interest_rate)

        single_payment_due_date = data.validated_data.get('single_payment_due_date', None)
        if single_payment_due_date is not None:
            c.drawRightString(10 * cm, (15 + 2.55) * cm, single_payment_due_date.strftime('%B %d'))
            c.drawString(11 * cm, (15 + 2.55) * cm, single_payment_due_date.strftime('%y'))

        single_payment_amount = data.validated_data.get('single_payment_amount', '')
        c.drawRightString(20.1 * cm, (15 + 2.55) * cm, single_payment_amount)

        init_y, delta_y = (13.7 + 2.55), 0.4
        for index, item in enumerate(data.validated_data.get('installment_monthly', [])):
            y = init_y - index * delta_y
            c.drawString(4.5 * cm, y * cm, item['term'])
            c.drawRightString(11.2 * cm, y * cm, item["amount_per_installment"])
            c.drawRightString(15.7 * cm, y * cm, item["total"])

        installment_total = data.validated_data.get('installment_total', '')
        c.drawRightString(20 * cm, (12.9 + 2.55) * cm, installment_total)

        pcci_tin = data.validated_data.get('pcci_tin', '')
        c.drawString(12.1 * cm, (9.1 + 2.55) * cm, pcci_tin)

        pcci_signatory = data.validated_data.get('pcci_signatory', '')
        c.drawString(11.9 * cm, (7.5 + 2.55) * cm, pcci_signatory.upper())

        pcci_signatory_position = data.validated_data.get('pcci_signatory_position', '')
        c.drawString(11.8 * cm, (5.85 + 2.55) * cm, pcci_signatory_position.upper())

        c.drawString(1.6 * cm, (5.85 + 2.55) * cm, borrower_name.upper())

        borrower_tin = data.validated_data.get('borrower_tin', '')
        c.drawString(2.1 * cm, (4.8 + 2.55) * cm, borrower_tin)

        if date is not None:
            c.drawRightString(6.9 * cm, (4.3 + 2.55) * cm, date.strftime('%B %d'))
            c.drawString(7.7 * cm, (4.3 + 2.55) * cm, date.strftime('%y'))

        c.setTitle(f'RP - Disclosure Statement')
        c.showPage()
        c.save()

        buffer.seek(0)
        return FileResponse(buffer, as_attachment=True, filename=f'REM - Disclosure Statement.pdf')
