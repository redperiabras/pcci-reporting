# -*- coding: utf-8 -*-
# @Time    : 2/6/21 10:45 PM
# @Author  : Red Periabras
# @File    : mortgage_declaration.py.py
# @Software: PyCharm


from io import BytesIO

from django.http import FileResponse
from reportlab.lib.pagesizes import legal
from reportlab.lib.units import cm
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen import canvas
from rest_framework import viewsets

from reports.serializers.rem.mortgage_declaration import MortgageDeclarationSerializer

FONT_SIZE = 8

import textwrap

pdfmetrics.registerFont(TTFont('F25_Bank_Printer', './reports/assets/fonts/F25_Bank_Printer.ttf'))


class REMMortgageDeclarationView(viewsets.ModelViewSet):
    serializer_class = MortgageDeclarationSerializer
    http_method_names = ['post']

    def create(self, request):
        data = self.serializer_class(data=request.data)

        if not data.is_valid(raise_exception=True):
            return

        buffer = BytesIO()
        c = canvas.Canvas(buffer, pagesize=legal)

        c.setFont('F25_Bank_Printer', size=FONT_SIZE)
        c.setStrokeColorRGB(169, 169, 169)

        pcci_province = data.validated_data['pcci_province']
        c.drawString(13.3 * cm, (26.4 + 5.05) * cm, pcci_province)

        date = data.validated_data['date']
        if date is not None:
            c.drawRightString(6.1 * cm, (26 + 5.05) * cm, "%d%s" % (date.day, "tsnrhtdd"[(date.day // 10 % 10 != 1) * (date.day % 10 < 4) * date.day % 10::4]))
            c.drawString(7.6 * cm, (26 + 5.05) * cm, date.strftime('%B'))
            c.drawString(13.4 * cm, (26 + 5.05) * cm, date.strftime('%y'))

        borrower_name = data.validated_data['borrower_name']
        #c.drawString(4.5 * cm, (25.5 + 5.05) * cm, borrower_name)

        #spouse_name = data.validated_data['spouse_name']
        #c.drawString(4.5 * cm, (25.1 + 5.05) * cm, spouse_name)

        wrap_text = textwrap.wrap(borrower_name, width=85)
        if len(borrower_name) > 1:
            c.drawString(4.5 * cm, (25.5 + 5.05) * cm, wrap_text[0])
            c.drawString(4.5 * cm, (25.1 + 5.05) * cm, wrap_text[1])
            if len(wrap_text) > 2:
                c.drawString(4.5 * cm, (24.7 + 5.05) * cm, wrap_text[2])
        else:
            c.drawString(4.5 * cm, (25.5 + 5.05) * cm, borrower_name)


        pcci_address = data.validated_data['pcci_address']
        if len(pcci_address) <= 65 :
            c.drawString(6.7 * cm, (21.85 + 5.05) * cm, pcci_address)
        else:
            c.drawString(6.7 * cm, (21.85 + 5.05) * cm, pcci_address[:65])
            c.drawString(4.5 * cm, (21.5 + 5.05) * cm, pcci_address[65:])

        c.setTitle('REM - Mortgage Declaration')
        c.showPage()
        c.save()

        buffer.seek(0)
        return FileResponse(buffer, as_attachment=True, filename=f'REM - Mortgage Declaration.pdf')
