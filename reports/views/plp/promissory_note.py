# -*- coding: utf-8 -*-
# @Time    : 1/28/21 9:28 PM
# @Author  : Red Periabras
# @Email   : redperiabras@gmail.com
# @Software: PyCharm

from io import BytesIO

from django.http import FileResponse
from reportlab.lib.units import inch
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen import canvas
from rest_framework import viewsets

from reports.serializers.plp.promissory_note import PromissoryNoteSerializer

PAPER_SIZE = (8.5 * inch, 13 * inch)
FONT_SIZE = 8

pdfmetrics.registerFont(TTFont('F25_Bank_Printer', './reports/assets/fonts/F25_Bank_Printer.ttf'))


class PLPPromissoryNoteView(viewsets.ModelViewSet):
    serializer_class = PromissoryNoteSerializer
    http_method_names = ['post']

    def create(self, request):
        data = self.serializer_class(data=request.data)

        if not data.is_valid(raise_exception=True):
            return

        buffer = BytesIO()
        c = canvas.Canvas(buffer, pagesize=PAPER_SIZE)

        c.setFont('F25_Bank_Printer', size=FONT_SIZE)
        c.setStrokeColorRGB(169, 169, 169)

        date = data.validated_data['date']
        if date is not None:
            c.drawString(5.11 * inch, 11.43 * inch, date.strftime('%B %d'))
            c.drawString(7.2 * inch, 11.43 * inch, date.strftime('%y'))

        amount_in_figures = data.validated_data['amount_in_figures']
        c.drawString(0.9 * inch, 11.05 * inch, amount_in_figures)

        amount_in_words = data.validated_data['amount_in_words']
        c.drawString(0.9 * inch, 10.5 * inch, amount_in_words.upper())

        c.drawString(6.1 * inch, 10.5 * inch, amount_in_figures)

        loanterm_in_words = data.validated_data['loanterm_in_words']
        c.drawString(2.3 * inch, 10.33 * inch, loanterm_in_words.upper())

        loanterm_in_figures = data.validated_data['loanterm_in_figures']
        c.drawString(6.1 * inch, 10.33 * inch, loanterm_in_figures)

        monthlyamort_in_words = data.validated_data['monthlyamort_in_words']
        c.drawString(2.65 * inch, 10.14 * inch, monthlyamort_in_words.upper())

        monthyamort_in_figures = data.validated_data['monthlyamort_in_figures']
        c.drawString(1.0 * inch, 9.96 * inch, monthyamort_in_figures)

        start_date = data.validated_data['start_date']
        if start_date is not None:
            c.drawString(3.6 * inch, 9.96 * inch, "%d%s" % (start_date.day,"tsnrhtdd"[(start_date.day//10%10!=1)*(start_date.day%10<4)*start_date.day%10::4]))
            c.drawString(0.9 * inch, 9.81 * inch, start_date.strftime("%B"))
            c.drawString(3.1 * inch, 9.81 * inch, start_date.strftime("%y"))

        maturity_date = data.validated_data['maturity_date']
        if maturity_date is not None:
            c.drawString(5.0 * inch, 9.81 * inch, maturity_date.strftime("%B %d"))
            c.drawString(7.35 * inch, 9.81 * inch, maturity_date.strftime("%y"))

        interest_in_words = data.validated_data['interest_in_words']
        c.drawString(4.55 * inch, 9.63 * inch, interest_in_words)

        interest_in_figures = data.validated_data['interest_in_figures']
        c.drawString(7.0 * inch, 9.63 * inch, interest_in_figures)

        proceeds_receiver = data.validated_data['proceeds_receiver']
        c.drawString(2.45 * inch, 5.58 * inch, proceeds_receiver)

        loan_purpose = data.validated_data['loan_purpose']
        c.drawString(3.03 * inch, 2.59 * inch, loan_purpose)

        makers_name = data.validated_data['makers_name']
        c.drawString(0.9 * inch, 1.39 * inch, makers_name.upper())

        comakers_name = data.validated_data['comakers_name']
        c.drawString(4.9 * inch, 1.39 * inch, comakers_name.upper())

        c.setTitle(f'PLP - Promissory Note')
        c.showPage()
        c.save()

        buffer.seek(0)
        return FileResponse(buffer, as_attachment=True, filename=f'PLP - Promissory Note.pdf')
