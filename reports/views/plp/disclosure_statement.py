# -*- coding: utf-8 -*-
# @Time    : 1/29/21 7:14 PM
# @Author  : Red Periabras
# @Email   : redentor.periabras@ringcentral.com
# @File    : disclosure_statement.py
# @Software: PyCharm

from io import BytesIO

from django.http import FileResponse
from reportlab.lib.units import inch
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen import canvas
from rest_framework import viewsets

from reports.serializers.plp.disclosure_statement import DisclosureStatementSerializer

PAPER_SIZE = (8.5 * inch, 13 * inch)
FONT_SIZE = 8

pdfmetrics.registerFont(TTFont('F25_Bank_Printer', './reports/assets/fonts/F25_Bank_Printer.ttf'))


class PLPDisclosureStatementView(viewsets.ModelViewSet):
    serializer_class = DisclosureStatementSerializer
    http_method_names = ['post']

    def create(self, request, *args, **kwwargs):
        data = self.serializer_class(data=request.data)

        if not data.is_valid(raise_exception=True):
            return

        buffer = BytesIO()
        c = canvas.Canvas(buffer, pagesize=PAPER_SIZE)

        c.setFont('F25_Bank_Printer', size=FONT_SIZE)
        c.setStrokeColorRGB(169, 169, 169)

        date = data.validated_data.get('date', None)

        borrower_name = data.validated_data.get('borrower_name', '')
        c.drawString(1.95 * inch, 11.56 * inch, borrower_name.upper())

        address = data.validated_data.get('address', '')
        c.drawString(1.3 * inch, 11.39 * inch, address)

        net_in_words = data.validated_data.get('net_in_words', '')
        c.drawString(2.0 * inch, 11.05 * inch, net_in_words.upper())

        net_in_figures = data.validated_data.get('net_in_figures', '')
        c.drawRightString(8 * inch, 11.05 * inch, net_in_figures)

        downpayment = data.validated_data.get('downpayment', '')
        c.drawRightString(8 * inch, 10.8 * inch, downpayment)

        unpaid_balance = data.validated_data.get('unpaid_balance', '')
        c.drawRightString(8 * inch, 10.59 * inch, unpaid_balance)

        net_service_fee = data.validated_data.get('net_service_fee', '')
        c.drawRightString(6.1 * inch, 10.28 * inch, net_service_fee)

        net_processing_fee = data.validated_data.get('net_processing_fee')
        c.drawRightString(6.1 * inch, 10.09 * inch, net_processing_fee)

        net_share_capital = data.validated_data.get('net_share_capital', '')
        c.drawRightString(6.1 * inch, 9.95 * inch, net_share_capital)

        net_membership_fee = data.validated_data['net_membership_fee']
        c.drawRightString(6.1 * inch, 9.8 * inch, net_membership_fee)

        net_documentary_fee = data.validated_data.get('net_documentary_fee', '')
        c.drawRightString(6.1 * inch, 9.61 * inch, net_documentary_fee)

        net_notarial_fee = data.validated_data.get('net_notarial_fee', '')
        c.drawRightString(6.1 * inch, 9.45 * inch, net_notarial_fee)

        init_y, delta_y = 9.25, 0.16
        for index, item in enumerate(data.validated_data.get('net_others', '')):
            y = init_y - index * delta_y
            c.drawString(1.53 * inch, y * inch, item['type'])
            c.drawRightString(6.1 * inch, y * inch, item["amount"])

        net_total_non_finance = data.validated_data.get('net_total_non_finance', '')
        c.drawRightString(8 * inch, 8.94 * inch, net_total_non_finance)

        net_to_be_financed = data.validated_data.get('net_to_be_financed', '')
        c.drawRightString(8 * inch, 8.8 * inch, net_to_be_financed)

        charge_interest = data.validated_data.get('charge_interest', '')
        c.drawRightString(1.9 * inch, 8.44 * inch, charge_interest)

        charge_start_date = data.validated_data.get('charge_start_date', '')
        c.drawString(2.7 * inch, 8.44 * inch, charge_start_date)

        charge_due_date = data.validated_data.get('charge_due_date', '')
        c.drawString(3.7 * inch, 8.44 * inch, charge_due_date)

        charge_total_interest = data.validated_data.get('charge_total_interest', '')
        c.drawRightString(8 * inch, 8.44 * inch, charge_total_interest)

        if data.validated_data['charge_is_compound']:
            c.drawString(1.1 * inch, 8.24 * inch, "/")

        if data.validated_data['charge_is_simple']:
            c.drawString(1.1 * inch, 8.09 * inch, "/")

        if data.validated_data['charge_is_semi_monthly']:
            c.drawString(2.05 * inch, 8.24 * inch, "/")

        if data.validated_data['charge_is_weekly']:
            c.drawString(2.05 * inch, 8.09 * inch, "/")

        if data.validated_data['charge_is_quarterly']:
            c.drawString(3.12 * inch, 8.24 * inch, "/")

        if data.validated_data['charge_is_monthly']:
            c.drawString(3.12 * inch, 8.09 * inch, "/")

        if data.validated_data['charge_is_semi_annual']:
            c.drawString(4 * inch, 8.23 * inch, "/")

        if data.validated_data['charge_is_annual']:
            c.drawString(4 * inch, 8.08 * inch, "/")

        percentage_charge_to_total_net = data.validated_data.get('percentage_charge_to_total_net', '')
        c.drawRightString(5.05 * inch, 7.93 * inch, percentage_charge_to_total_net)

        effective_interest_rate = data.validated_data.get('effective_interest_rate', '')
        c.drawRightString(4.45 * inch, 7.25 * inch, effective_interest_rate)

        single_payment_due_date = data.validated_data.get('single_payment_due_date', None)
        if single_payment_due_date is not None:
            c.drawString(2.43 * inch, 6.42 * inch, single_payment_due_date.strftime('%B %d'))
            c.drawString(4.38 * inch, 6.42 * inch, single_payment_due_date.strftime('%y'))

        single_payment_amount = data.validated_data.get('single_payment_amount', '')
        c.drawRightString(8 * inch, 6.42 * inch, single_payment_amount)

        init_y, delta_y = 5.89, 0.19
        for index, item in enumerate(data.validated_data.get('installment_monthly', [])):
            y = init_y - index * delta_y
            c.drawString(1.9 * inch, y * inch, item['term'])
            c.drawRightString(4.5 * inch, y * inch, item["amount_per_installment"])
            c.drawRightString(6.25 * inch, y * inch, item["total"])

        installment_total = data.validated_data.get('installment_total', '')
        c.drawRightString(8 * inch, 5.63 * inch, installment_total)

        pcci_tin = data.validated_data.get('pcci_tin', '')
        c.drawString(4.9 * inch, 4.04 * inch, pcci_tin)

        pcci_signatory = data.validated_data.get('pcci_signatory', '')
        c.drawString(4.8 * inch, 3.5 * inch, pcci_signatory.upper())

        pcci_signatory_position = data.validated_data.get('pcci_signatory_position', '')
        c.drawString(4.8 * inch, 2.83 * inch, pcci_signatory_position.upper())

        c.drawString(0.8 * inch, 2.83 * inch, borrower_name.upper())

        borrower_tin = data.validated_data.get('borrower_tin', '')
        c.drawString(0.9 * inch, 2.34 * inch, borrower_tin)

        if date is not None:
            c.drawString(1.0 * inch, 2.16 * inch, date.strftime('%B %d'))
            c.drawString(3.05 * inch, 2.16 * inch, date.strftime('%y'))

        c.setTitle(f'PLP - Disclosure Statement')
        c.showPage()
        c.save()

        buffer.seek(0)
        return FileResponse(buffer, as_attachment=True, filename=f'PLP - Disclosure Statement.pdf')
