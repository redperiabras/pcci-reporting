# -*- coding: utf-8 -*-
# @Time    : 1/30/21 11:02 PM
# @Author  : Red Periabras
# @File    : authority_to_deduct.py
# @Software: PyCharm

from io import BytesIO

from django.http import FileResponse
from reportlab.lib.units import cm, inch
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen import canvas
from rest_framework import viewsets

from reports.serializers.plp.authority_to_deduct import AuthorityToDeductSerializer

PAPER_SIZE = (8.5 * inch, 11 * inch)
FONT_SIZE = 8

pdfmetrics.registerFont(TTFont('F25_Bank_Printer', './reports/assets/fonts/F25_Bank_Printer.ttf'))


class PLPAuthorityToDeductView(viewsets.ModelViewSet):
    serializer_class = AuthorityToDeductSerializer
    http_method_names = ['post']

    def create(self, request):
        data = self.serializer_class(data=request.data)

        if not data.is_valid(raise_exception=True):
            return

        buffer = BytesIO()
        c = canvas.Canvas(buffer, pagesize=PAPER_SIZE)

        c.setFont('F25_Bank_Printer', size=FONT_SIZE)
        c.setStrokeColorRGB(169, 169, 169)

        monthly_amort = data.validated_data.get('monthly_amort', '')
        if len(monthly_amort) <= 9:
            c.drawString(16.2 * cm, 21.65 * cm, monthly_amort)
        else:
            c.drawString(16.2 * cm, 21.65 * cm, monthly_amort[:9])
            c.drawString(4.6 * cm, 21.2 * cm, monthly_amort[9:])

        source_amort = data.validated_data.get('source_amort', '')
        c.drawString(5.4 * cm, 20.7 * cm, source_amort)

        agent_name = data.validated_data.get('agent_name', '')
        c.drawString(12.6 * cm, 20.7 * cm, agent_name)

        loan_term = data.validated_data.get('loan_term', '')
        c.drawString(9.35 * cm, 19.8 * cm, loan_term)

        start_date = data.validated_data['start_date']
        if len(start_date) <= 10:
            c.drawString(15.8 * cm, 19.8 * cm,  start_date)
        else:

            c.drawString(15.8 * cm, 19.8 * cm, start_date[:10])
            c.drawString(4.5 * cm, 19.3 * cm, start_date[10:])

        loan_amount_in_words = data.validated_data.get('loan_amount_in_words', '')
        if len(loan_amount_in_words) <= 4:
            c.drawString(17.1 * cm, 19.3 * cm,   loan_amount_in_words)
        else:
            c.drawString(17.1 * cm, 19.3 * cm, loan_amount_in_words[:4])
            c.drawString(4.5 * cm, 18.8 * cm, loan_amount_in_words[4:])

        loan_amount_in_figures = data.validated_data.get('loan_amount_in_figures', '')
        c.drawRightString(10.7 * cm, 18.8 * cm, loan_amount_in_figures)

        c.drawString(7.3 * cm, 18.35 * cm, agent_name)
        c.drawString(7.9 * cm, 17.45 * cm, agent_name)

        purchase_description = data.validated_data.get('purchase_description', '')
        c.drawString(13.9 * cm, 13.7 * cm, purchase_description)

        c.drawString(6.85 * cm, 10.4 * cm, loan_amount_in_words)
        c.drawRightString(17.7 * cm, 10.4 * cm, loan_amount_in_figures)

        borrower_name = data.validated_data.get('borrower_name', '')
        c.drawString(10.6 * cm, 9.95 * cm, borrower_name)

        c.drawString(4.5 * cm, 9.5 * cm, purchase_description)

        invoice_no = data.validated_data.get('invoice_no', '')
        c.drawRightString(17.9 * cm, 9.5 * cm, invoice_no)

        invoice_date = data.validated_data['invoice_date']
        c.drawString(5.5 * cm, 9 * cm, invoice_date)

        authorized_to_pay_directly = data.validated_data.get('authorized_to_pay_directly', '')
        c.drawString(8.6 * cm, 8.53 * cm, authorized_to_pay_directly)

        borrower_name = data.validated_data.get('borrower_name', '')
        c.drawString(13.8 * cm, 6.9 * cm, borrower_name)

        borrower_payroll_no = data.validated_data.get('borrower_payroll_no', '')
        c.drawString(13.8 * cm, 6.4 * cm, borrower_payroll_no)

        borrower_pay_grade = data.validated_data.get('borrower_pay_grade', '')
        c.drawString(13.8 * cm, 5.9 * cm, borrower_pay_grade)

        c.setTitle(f'PLP - Authority to Deduct')
        c.showPage()
        c.save()

        buffer.seek(0)
        return FileResponse(buffer, as_attachment=True, filename=f'PLP - Authority to Deduct.pdf')
