# -*- coding: utf-8 -*-
# @Time    : 1/27/21 10:54 PM
# @Author  : Red Periabras
# @Email   : redperiabras@gmail.com
# @Software: PyCharm

from io import BytesIO

from django.http import FileResponse
from reportlab.lib.pagesizes import letter
from reportlab.lib.units import inch
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen import canvas
from rest_framework import viewsets

from ..serializers.official_receipt import OfficialReceiptSerializer

import textwrap

FONT_SIZE = 8

pdfmetrics.registerFont(TTFont('F25_Bank_Printer', './reports/assets/fonts/F25_Bank_Printer.ttf'))

class OfficialReceiptView(viewsets.ModelViewSet):
    serializer_class = OfficialReceiptSerializer
    http_method_names = ['post']

    def create(self, request):

        data = self.serializer_class(data=request.data)

        if not data.is_valid(raise_exception=True):
            return

        buffer = BytesIO()
        c = canvas.Canvas(buffer, pagesize=letter)

        c.setFont('F25_Bank_Printer', size=FONT_SIZE)
        c.setStrokeColorRGB(169, 169, 169)

        date = data.validated_data.get('date', '')
        if type(date) is not str:
            c.drawString(6.25 * inch, (3.45 + 5.5) * inch, date.strftime('%B %d'))
            c.drawString(7.81 * inch, (3.45 + 5.5) * inch, date.strftime('%y'))

        or_number = data.validated_data.get('or_number', '')
        c.drawString(6.8 * inch, (4.2 + 5.5) * inch, or_number)

        customer_name = data.validated_data.get('customer_name', '')
        c.drawString(4.5 * inch, (3.21 + 5.5) * inch, customer_name)

        tin = data.validated_data.get('tin', '')
        c.drawString(3.8 * inch, (2.95 + 5.5) * inch, tin)

        business_style = data.validated_data.get('business_style', '')
        c.drawString(6.6 * inch, (2.95 + 5.5) * inch, business_style)

        address = data.validated_data.get('address', '')
        c.drawString(3.9 * inch, (2.65 + 5.5) * inch, address)

        amount_in_words = data.validated_data.get('amount_in_words', '')
        #if len(amount_in_words) <= 55:
        #    c.drawString(4.0 * inch, (2.4 + 5.5) * inch, amount_in_words)
        #else:
        #    c.drawString(4.0 * inch, (2.4 + 5.5) * inch, amount_in_words[:55])
        #    c.drawString(3.4 * inch, (2.15 + 5.5) * inch, amount_in_words[55:])

        if len(amount_in_words) > 55:
            wrap_text = textwrap.wrap(amount_in_words, width=55)
            c.drawString(4.0 * inch, (2.4 + 5.5) * inch, wrap_text[0])
            c.drawString(3.4 * inch, (2.15 + 5.5) * inch, wrap_text[1])
        else:
            c.drawString(4.0 * inch, (2.4 + 5.5) * inch, amount_in_words)

        amount_in_figures = data.validated_data.get('amount_in_figures', '')
        c.drawRightString(4.7 * inch, (1.85 + 5.5) * inch, amount_in_figures)

        account_number = data.validated_data.get('account_number', '')
        c.drawString(5.8 * inch, (1.57 + 5.5) * inch, account_number)

        description = data.validated_data.get('description', '')
        #if len(description) <= 33:
        #    c.drawString(3.4 * inch, (1.4 + 5.5) * inch, description)
        #else:
        #    c.drawString(3.4 * inch, (1.4 + 5.5) * inch, description[:33])
        #    c.drawString(3.4 * inch, (1.27 + 5.5) * inch, description[33:])
        if len(description) > 33:
            wrap_text = textwrap.wrap(description, width=33)
            c.drawString(3.4 * inch, (1.4 + 5.5) * inch, wrap_text[0])
            c.drawString(3.4 * inch, (1.27 + 5.5) * inch, wrap_text[1])
        else:
            c.drawString(3.4 * inch, (1.4 + 5.5) * inch, description)

        payment_particulars = data.validated_data.get('payment_particulars', [])

        start_x, x1, = (4.33 + 5.5), .205
        for index, item in enumerate(payment_particulars):
            x = (start_x - index * x1) * inch # for rows
            c.drawString(0.68 * inch, x, item['particular'])
            c.drawString(1.34 * inch, x, item['acct_code'])
            c.drawRightString(3.15 * inch, x, item['amount'])

        c.drawRightString(3.15 * inch, (3.09 + 5.5) * inch, amount_in_figures)

        cash_payment = data.validated_data.get('cash_payment', '')
        c.drawRightString(3.15 * inch, (2.35 + 5.5) * inch, cash_payment)

        check_payment = data.validated_data.get('check_payment', [])

        start_x, x1 = (1.9 + 5.5), .2
        for index, item in enumerate(check_payment):
            x = (start_x - index * x1) * inch
            c.drawString(0.68 * inch, x, item['check_number'])
            c.drawRightString(3.15 * inch, x, item['amount'])

        c.drawRightString(3.15 * inch, (0.9 + 5.5) * inch, amount_in_figures)

        pcci_signatory = data.validated_data['pcci_signatory']
        c.drawString(6.2 * inch, (0.9 + 5.5) * inch, pcci_signatory)

        c.setTitle(f'Official Receipt')
        c.showPage()
        c.save()

        buffer.seek(0)
        return FileResponse(buffer, as_attachment=True, filename=f'Official Receipt.pdf')
