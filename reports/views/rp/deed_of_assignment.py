# -*- coding: utf-8 -*-
# @Time    : 2/6/21 3:07 PM
# @Author  : Red Periabras
# @File    : deed_of_assignment.py
# @Software: PyCharm

from io import BytesIO

from django.http import FileResponse
from reportlab.lib import colors
from reportlab.lib.pagesizes import legal
from reportlab.lib.units import cm
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen import canvas
from reportlab.platypus import Table, TableStyle
from rest_framework import viewsets

from reports.serializers.rp.deed_of_assignment import DeedOfAssignmentSerializer

FONT_SIZE = 9

pdfmetrics.registerFont(TTFont('F25_Bank_Printer', './reports/assets/fonts/F25_Bank_Printer.ttf'))


class RPDeedOfAssignment(viewsets.ModelViewSet):
    serializer_class = DeedOfAssignmentSerializer
    http_method_names = ['post']

    def create(self, request):
        data = self.serializer_class(data=request.data)

        if not data.is_valid(raise_exception=True):
            return

        buffer = BytesIO()
        c = canvas.Canvas(buffer, pagesize=legal)

        c.setFont('F25_Bank_Printer', size=FONT_SIZE)
        c.setStrokeColorRGB(169, 169, 169)

        maturity_value_in_words = data.validated_data['maturity_value_in_words']
        if len(maturity_value_in_words) <= 50:
            c.drawString(8.9 * cm, (27.4 + 2.55) * cm, maturity_value_in_words)
        else:
            c.drawString(8.9 * cm, (27.4 + 2.55) * cm, maturity_value_in_words[:50])
            c.drawString(2.0 * cm, (26.9 + 2.55) * cm, maturity_value_in_words[50:])

        maturity_value_in_figures = data.validated_data['maturity_value_in_figures']
        c.drawRightString(14.3 * cm, (26.9 + 2.55) * cm, maturity_value_in_figures)

        borrower_name = data.validated_data['borrower_name']
        c.drawString(2.0 * cm, (25.5 + 2.55) * cm, borrower_name)

        by_virtue_of_the_laws_of = data.validated_data['by_virtue_of_the_laws_of']
        c.drawString(6.7 * cm, (25.0 + 2.55) * cm, by_virtue_of_the_laws_of)

        borrower_address = data.validated_data['borrower_address']
        c.drawString(2.0 * cm, (24.5 + 2.55) * cm, borrower_address)

        borrower_position = data.validated_data['borrower_position']
        borrower_signatory = data.validated_data['borrower_signatory']
        c.drawString(2.0 * cm, (24.0 + 2.55) * cm, f'{borrower_position}, {borrower_signatory}')

        borrower_signatory_address = data.validated_data['borrower_signatory_address']
        c.drawString(4.1 * cm, (23.6 + 2.55) * cm, borrower_signatory_address)

        pcci_name = data.validated_data['pcci_name']
        c.drawString(2.9 * cm, (23.1 + 2.55) * cm, pcci_name)

        pcci_address = data.validated_data['pcci_address']
        c.drawString(11.7 * cm, (22.6 + 2.55) * cm, pcci_address)

        rights_and_interests_table = [['Maturity Value', 'Value Date', 'Maturity Date', 'Drawer Bank', 'Check #', 'Check Issuer']]
        rights_and_interests = data.validated_data['rights_and_interests']
        for item in rights_and_interests:
            rights_and_interests_table.append([
                item['maturity_value'],
                item['value_date'],
                item['maturity_date'],
                item['drawer_bank'],
                item['check_number'],
                item['check_issuer']
            ])
        t = Table(rights_and_interests_table, colWidths=[3*cm]*5)
        t.setStyle(TableStyle([
            ('FONTNAME', (0, 0), (-1, -1), 'F25_Bank_Printer'),
            ('FONTSIZE', (0, 0), (-1, -1), 8),
        ]))


        t.wrapOn(c, 400, 100)
        t.drawOn(c, 2 * cm, 22.2 * cm)

        maturity_value = data.validated_data['maturity_value']
        c.drawString(2.0 * cm, (16.1 + 2.55) * cm, maturity_value)

        c.drawString(3.1 * cm, (12.8 + 2.55) * cm, pcci_name)

        date = data.validated_data['date']
        c.drawString(8.4 * cm, (8.1 + 2.55) * cm, date)

        c.drawString(12.9 * cm, (7.4 + 2.55) * cm, borrower_name)

        c.drawString(12.9 * cm, (5.4 + 2.55) * cm, borrower_signatory)

        witness_1 = data.validated_data['witness_1']
        c.drawString(2.3 * cm, (3.2 + 2.55) * cm, witness_1)

        witness_2 = data.validated_data['witness_2']
        c.drawString(12.3 * cm, (3.2 + 2.55) * cm, witness_2)

        c.setTitle('RP - Deed of Assignment')
        c.showPage()
        c.save()

        buffer.seek(0)
        return FileResponse(buffer, as_attachment=True, filename='RP - Deed of Assignment.pdf')
