# -*- coding: utf-8 -*-
# @Time    : 1/28/21 9:28 PM
# @Author  : Red Periabras
# @Email   : redperiabras@gmail.com
# @Software: PyCharm

from io import BytesIO

from django.http import FileResponse
from reportlab.lib.pagesizes import legal
from reportlab.lib.units import cm
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen import canvas
from rest_framework import viewsets

from reports.serializers.rp.acknowledgment import AcknowledgmentSerializer

FONT_SIZE = 8

pdfmetrics.registerFont(TTFont('F25_Bank_Printer', './reports/assets/fonts/F25_Bank_Printer.ttf'))


class RPAcknowledgment(viewsets.ModelViewSet):
    serializer_class = AcknowledgmentSerializer
    http_method_names = ['post']

    def create(self, request):
        data = self.serializer_class(data=request.data)

        if not data.is_valid(raise_exception=True):
            return

        buffer = BytesIO()
        c = canvas.Canvas(buffer, pagesize=legal)

        c.setFont('F25_Bank_Printer', size=FONT_SIZE)
        c.setStrokeColorRGB(169, 169, 169)

        borrower_signatory = data.validated_data['borrower_signatory']
        c.drawString(8.8 * cm, (25.6 + 2.7) * cm, borrower_signatory)

        borrower_id_no = data.validated_data['borrower_id_no']
        c.drawString(6.7 * cm, (25.1 + 2.7) * cm, borrower_id_no)

        borrower_id_place_issue = data.validated_data['borrower_id_place_issue']
        c.drawString(12.6 * cm, (25.1 + 2.7) * cm, borrower_id_place_issue)

        borrower_id_date_issue = data.validated_data['borrower_id_date_issue']
        c.drawString(2.5 * cm, (24.65 + 2.7) * cm, borrower_id_date_issue)

        pcci_signatory = data.validated_data['pcci_signatory']
        c.drawString(10.5 * cm, (24.65 + 2.7) * cm, pcci_signatory)

        pcci_id_no = data.validated_data['pcci_id_no']
        c.drawString(6.4 * cm, (24.2 + 2.7) * cm, pcci_id_no)

        pcci_id_place_issue = data.validated_data['pcci_id_place_issue']
        c.drawString(12.6 * cm, (24.2 + 2.7) * cm, pcci_id_place_issue)

        pcci_id_date_issue = data.validated_data['pcci_id_date_issue']
        c.drawString(2.5 * cm, (23.75 + 2.7) * cm, pcci_id_date_issue)

        borrower_position = data.validated_data['borrower_position']
        c.drawString(12.6 * cm, (23.7 + 2.7) * cm, borrower_position)

        pcci_position = data.validated_data['pcci_position']
        c.drawString(2. * cm, (23.3 + 2.7) * cm, pcci_position)

        pcci_address = data.validated_data['pcci_address']
        c.drawString(10.2 * cm, (23.3 + 2.7) * cm, pcci_address)

        c.drawString(5 * cm, (22.8 + 2.7) * cm, pcci_id_no)

        c.drawString(9.3 * cm, (22.8 + 2.7) * cm, pcci_id_place_issue)

        c.setTitle('RP - Acknowledgment')
        c.showPage()
        c.save()

        buffer.seek(0)
        return FileResponse(buffer, as_attachment=True, filename='RP - Acknowledgment.pdf')
