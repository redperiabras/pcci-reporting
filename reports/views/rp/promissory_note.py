# -*- coding: utf-8 -*-
# @Time    : 1/28/21 9:28 PM
# @Author  : Red Periabras
# @Email   : redperiabras@gmail.com
# @Software: PyCharm

from io import BytesIO

from django.http import FileResponse
from reportlab.lib.pagesizes import legal
from reportlab.lib.units import cm
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen import canvas
from rest_framework import viewsets

from reports.serializers.rp.promissory_note import PromissoryNoteSerializer

FONT_SIZE = 8

pdfmetrics.registerFont(TTFont('F25_Bank_Printer', './reports/assets/fonts/F25_Bank_Printer.ttf'))


class RPPromissoryNoteView(viewsets.ModelViewSet):
    serializer_class = PromissoryNoteSerializer
    http_method_names = ['post']

    def create(self, request):
        data = self.serializer_class(data=request.data)

        if not data.is_valid(raise_exception=True):
            return

        buffer = BytesIO()
        c = canvas.Canvas(buffer, pagesize=legal)

        c.setFont('F25_Bank_Printer', size=FONT_SIZE)
        c.setStrokeColorRGB(169, 169, 169)

        date = data.validated_data['date']
        if date is not None:
            c.drawRightString(18.4 * cm, (29.5 + 2.55) * cm, date.strftime('%B %d'))
            c.drawString(19.3 * cm, (29.5 + 2.55) * cm, date.strftime('%y'))

        amount_in_figures = data.validated_data['amount_in_figures']
        c.drawString(1.8 * cm, (29.0 + 2.55)  * cm, amount_in_figures)

        amount_in_words = data.validated_data['amount_in_words']
        c.drawString(1.4 * cm, (27.75 + 2.55)  * cm, amount_in_words.upper())

        c.drawRightString(19.95 * cm, (27.75 + 2.55)  * cm, amount_in_figures)

        loanterm_in_words = data.validated_data['loanterm_in_words']
        c.drawString(5.0 * cm, (27.3 + 2.55) * cm, loanterm_in_words.upper())

        loanterm_in_figures = data.validated_data['loanterm_in_figures']
        c.drawRightString(17.7 * cm, (27.3 + 2.55) * cm, loanterm_in_figures)

        monthlyamort_in_words = data.validated_data['monthlyamort_in_words']
        c.drawString(5.3 * cm, (26.9 + 2.55) * cm, monthlyamort_in_words.upper())

        monthyamort_in_figures = data.validated_data['monthlyamort_in_figures']
        c.drawRightString(4.25 * cm, (26.5 + 2.55) * cm, monthyamort_in_figures)

        start_date = data.validated_data['start_date']
        if start_date is not None:
            c.drawString(7.6 * cm, (26.5 + 2.55) * cm, "%d%s" % (start_date.day,"tsnrhtdd"[(start_date.day//10%10!=1)*(start_date.day%10<4)*start_date.day%10::4]))
            c.drawString(1.45 * cm, (26.1 + 2.55) * cm, start_date.strftime("%B"))
            c.drawString(7.0 * cm, (26.1 + 2.55) * cm, start_date.strftime("%y"))

        maturity_date = data.validated_data['maturity_date']
        if maturity_date is not None:
            c.drawRightString(18.4 * cm, (26.1 + 2.55) * cm, maturity_date.strftime("%B %d"))
            c.drawString(19.4 * cm, (26.1 + 2.55) * cm, maturity_date.strftime("%y"))

        interest_in_words = data.validated_data['interest_in_words']
        c.drawString(7.4 * cm, (25.7 + 2.55) * cm, interest_in_words)

        interest_in_figures = data.validated_data['interest_in_figures']
        c.drawRightString(19.8 * cm, (25.7 + 2.55) * cm, interest_in_figures)

        late_pay_percent = data.validated_data['late_pay_percent']
        c.drawRightString(12.55 * cm, (14.5 + 2.55) * cm, late_pay_percent)

        proceeds_receiver = data.validated_data['proceeds_receiver']
        c.drawString(1.4 * cm, (9.3 + 2.55) * cm, proceeds_receiver)

        loan_purpose = data.validated_data['loan_purpose']
        c.drawString(5.4 * cm, (5.9 + 2.55) * cm, loan_purpose)

        witness_1 = data.validated_data['witness_1']
        c.drawString(1.75 * cm,(4.1 + 2.55) * cm, witness_1.upper())

        witness_2 = data.validated_data['witness_2']
        c.drawString(1.75 * cm, (2.7 + 2.55) * cm, witness_2.upper())

        witness_3 = data.validated_data['witness_3']
        c.drawString(13.2 * cm,(4.1 + 2.55) * cm, witness_3.upper())

        witness_4 = data.validated_data['witness_4']
        c.drawString(13.2 * cm, (2.7 + 2.55) * cm, witness_4.upper())

        c.setTitle(f'RP - Promissory Note')
        c.showPage()
        c.save()

        buffer.seek(0)
        return FileResponse(buffer, as_attachment=True, filename=f'RP - Promissory Note.pdf')
