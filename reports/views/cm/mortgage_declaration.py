# -*- coding: utf-8 -*-
# @Time    : 2/5/21 7:58 PM
# @Author  : Red Periabras
# @File    : mortgage_declaration.py
# @Software: PyCharm


from io import BytesIO

from django.http import FileResponse
from reportlab.lib import colors
from reportlab.lib.pagesizes import letter
from reportlab.lib.units import cm
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen import canvas
from reportlab.platypus import Table, TableStyle
from rest_framework import viewsets

from reports.serializers.cm.mortgage_declaration import MortgageDeclarationSerializer

import textwrap

FONT_SIZE = 7

pdfmetrics.registerFont(TTFont('F25_Bank_Printer', './reports/assets/fonts/F25_Bank_Printer.ttf'))


class MortgageDeclarationView(viewsets.ModelViewSet):
    serializer_class = MortgageDeclarationSerializer
    http_method_names = ['post']

    def create(self, request):
        data = self.serializer_class(data=request.data)

        if not data.is_valid(raise_exception=True):
            return

        buffer = BytesIO()
        c = canvas.Canvas(buffer, pagesize=letter)

        c.setFont('F25_Bank_Printer', size=FONT_SIZE)
        c.setStrokeColorRGB(169, 169, 169)

        date = data.validated_data['date']
        if date is not None:
            c.drawString(6.6 * cm, (20.3 + 5) * cm, "%d%s" % (date.day,"tsnrhtdd"[(date.day//10%10!=1)*(date.day%10<4)*date.day%10::4]))
            c.drawString(11.4 * cm, (20.3 + 5) * cm, date.strftime('%B'))
            c.drawString(19.4 * cm, (20.3 + 5) * cm, date.strftime('%y'))

        borrower_name = data.validated_data['borrower_name']
        #if len(borrower_name) <= 96:
        #    c.drawString(1.5 * cm, (19.9 + 5) * cm, borrower_name)
        #else:
        #    c.drawString(1.5 * cm, (19.9 + 5) * cm, borrower_name[:96])
        #    c.drawString(1.05 * cm, (19.35 + 5) * cm, borrower_name[96:])

        if len(borrower_name) > 96:
            wrap_text = textwrap.wrap(borrower_name, width=96)
            c.drawString(1.5 * cm, (19.9 + 5) * cm, wrap_text[0])
            c.drawString(1.05 * cm, (19.35 + 5) * cm, wrap_text[1])
        else:
            c.drawString(1.5 * cm, (19.9 + 5) * cm, borrower_name)



        principal_office = data.validated_data['principal_office']
        if len(principal_office) <= 41:
            c.drawString(12.3 * cm, (18.5 + 5) * cm, principal_office)
        else:
            c.drawString(12.3 * cm, (18.5 + 5) * cm, principal_office[:41])
            c.drawString(1.05 * cm, (18.0 + 5) * cm, principal_office[41:])

        city = data.validated_data['city']
        c.drawString(6.3 * cm, (16.25 + 5) * cm, city)

        province = data.validated_data['province']
        c.drawString(13.7 * cm, (16.25 + 5) * cm, province)

        vehicle_informations = [['MOTOR NO.', 'SERIAL NO.', 'PLATE NO.', 'MAKE/MODEL']]
        properties = data.validated_data['properties']
        for item in properties:
            vehicle_informations.append([
                item['motor_no'],
                item['serial_no'],
                item['plate_no'],
                item['make']
            ])
        t = Table(vehicle_informations, colWidths=[3.5*cm]*5)
        t.setStyle(TableStyle([
            ('ALIGN', (0, 0), (-1, 0), "CENTER"),
            ('FONTNAME', (0, 0), (-1, -1), 'F25_Bank_Printer'),
            ('FONTSIZE', (0, 0), (-1, -1), 8),
        ]))

        t.wrapOn(c, 400, 100)
        t.drawOn(c, 3.8 * cm, (14.1 + 5) * cm)

        c.setTitle(f'CM - Mortgage Declaration')
        c.showPage()
        c.save()

        buffer.seek(0)
        return FileResponse(buffer, as_attachment=True, filename=f'CM - Mortgage Declaration.pdf')
