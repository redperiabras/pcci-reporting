# -*- coding: utf-8 -*-
# @Time    : 2/5/21 9:07 PM
# @Author  : Red Periabras
# @File    : mortgage_witnesses.py
# @Software: PyCharm

from io import BytesIO

from django.http import FileResponse
from reportlab.lib.pagesizes import letter
from reportlab.lib.units import cm
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen import canvas
from rest_framework import viewsets

from reports.serializers.cm.mortgage_witnesses import MortgageWitnessesSerializer

FONT_SIZE = 7

pdfmetrics.registerFont(TTFont('F25_Bank_Printer', './reports/assets/fonts/F25_Bank_Printer.ttf'))


class MortgageWitnessesView(viewsets.ModelViewSet):
    serializer_class = MortgageWitnessesSerializer
    http_method_names = ['post']

    def create(self, request):
        data = self.serializer_class(data=request.data)

        if not data.is_valid(raise_exception=True):
            return

        buffer = BytesIO()
        c = canvas.Canvas(buffer, pagesize=letter)

        c.setFont('F25_Bank_Printer', size=FONT_SIZE)
        c.setStrokeColorRGB(169, 169, 169)

        amount_in_words = data.validated_data['amount_in_words']
        c.drawString(1.75 * cm, (33.43 - 17.4) * cm, amount_in_words)

        amount_in_figures = data.validated_data['amount_in_figures']
        c.drawRightString(20.1 * cm, (33.43 - 17.4) * cm, amount_in_figures)

        date = data.validated_data['date']
        if date is not None:
            c.drawRightString(18.8 * cm, (33.13 - 17.4) * cm, date.strftime('%B %d'))
            c.drawString(19.7 * cm, (33.13 - 17.4) * cm, date.strftime('%y'))

        borrower_name = data.validated_data['borrower_name']
        c.drawString(1.9 * cm, (32.75 - 17.4) * cm, borrower_name)

        if date is not None:
            c.drawString(4 * cm, (30.7 - 17.4) * cm, date.strftime('%B %d'))
            c.drawString(10.8 * cm, (30.7 - 17.4) * cm, date.strftime('%y'))

        pcci_city = data.validated_data['pcci_city']
        c.drawString(11.8 * cm, (30.7 - 17.4) * cm, pcci_city)

        c.drawString(12.5 * cm, (28.55 - 17.4) * cm, borrower_name)

        pcci_tin = data.validated_data['pcci_tin']
        c.drawString(14.8 * cm, (27.4 - 17.4) * cm, pcci_tin)

        witness_1 = data.validated_data['witness_1']
        c.drawString(2.3 * cm, (25.6 - 17.4) * cm, witness_1)

        witness_2 = data.validated_data['witness_2']
        c.drawString(12.5 * cm, (25.6 - 17.4) * cm, witness_2)

        c.drawString(2.9 * cm, (24.5 - 17.4) * cm, borrower_name)

        c.setTitle(f'CM - Mortgage Witnesses')
        c.showPage()
        c.save()

        buffer.seek(0)
        return FileResponse(buffer, as_attachment=True, filename=f'CM - Mortgage Witnesses.pdf')
