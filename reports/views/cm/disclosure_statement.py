# -*- coding: utf-8 -*-
# @Time    : 1/28/21 9:28 PM
# @Author  : Red Periabras
# @Software: PyCharm

from io import BytesIO

from django.http import FileResponse
from reportlab.lib.pagesizes import letter
from reportlab.lib.units import cm
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen import canvas
from rest_framework import viewsets

from reports.serializers.cm.disclosure_statement import DisclosureStatementSerializer

FONT_SIZE = 7

pdfmetrics.registerFont(TTFont('F25_Bank_Printer', './reports/assets/fonts/F25_Bank_Printer.ttf'))

class CMDisclosureStatmentView(viewsets.ModelViewSet):
    serializer_class = DisclosureStatementSerializer
    http_method_names = ['post']

    def create(self, request):
        data = self.serializer_class(data=request.data)

        if not data.is_valid(raise_exception=True):
            return

        buffer = BytesIO()
        c = canvas.Canvas(buffer, pagesize=letter)

        c.setFont('F25_Bank_Printer', size=FONT_SIZE)
        c.setStrokeColorRGB(169, 169, 169)

        date = data.validated_data['date']

        borrower_name = data.validated_data['borrower_name']
        c.drawString(4.6 * cm, (42.7 - 17.5) * cm, borrower_name.upper())

        address = data.validated_data['address']
        c.drawString(3.0 * cm, (42.3 - 17.5) * cm, address)

        net_in_words = data.validated_data.get('net_in_words', '')
        c.drawString(4.7 * cm, (41.65 - 17.5) * cm, net_in_words.upper())

        net_in_figures = data.validated_data.get('net_in_figures', '')
        c.drawRightString(19.9 * cm, (41.65 - 17.5) * cm, net_in_figures)

        downpayment = data.validated_data.get('downpayment', '')
        c.drawRightString(19.9 * cm, (40.9 - 17.5) * cm, downpayment)

        unpaid_balance = data.validated_data.get('unpaid_balance', '')
        c.drawRightString(19.9 * cm, (40.5 - 17.5) * cm, unpaid_balance)

        net_processing_fee = data.validated_data.get('net_processing_fee')
        c.drawRightString(15.3 * cm, (39.9 - 17.5) * cm, net_processing_fee)

        net_appraisal_fee = data.validated_data.get('net_appraisal_fee', '')
        c.drawRightString(15.3 * cm, (39.5 - 17.5) * cm, net_appraisal_fee)

        net_registration_fee = data.validated_data.get('net_registration_fee', '')
        c.drawRightString(15.3 * cm, (39.2 - 17.5) * cm, net_registration_fee)

        net_documentary_fee = data.validated_data.get('net_documentary_fee', '')
        c.drawRightString(15.3 * cm, (38.9 - 17.5) * cm, net_documentary_fee)

        net_notarial_fee = data.validated_data.get('net_notarial_fee', '')
        c.drawRightString(15.3 * cm, (38.6 - 17.5) * cm, net_notarial_fee)

        net_service_fee = data.validated_data.get('net_service_fee', '')
        c.drawRightString(15.3 * cm, (38.2 - 17.5) * cm, net_service_fee)

        net_share_capital = data.validated_data.get('net_share_capital', '')
        c.drawRightString(15.3 * cm, (37.9 - 17.5) * cm, net_share_capital)

        init_y, delta_y = (37.5 - 17.5), 0.32
        for index, item in enumerate(data.validated_data.get('net_others', '')):
            y = init_y - index * delta_y
            c.drawString(3.4 * cm, y * cm, item['type'])
            c.drawRightString(15.3 * cm, y * cm, item["amount"])

        net_total_non_finance = data.validated_data.get('net_total_non_finance', '')
        c.drawRightString(19.9 * cm, (36.75 - 17.5) * cm, net_total_non_finance)

        net_to_be_financed = data.validated_data.get('net_to_be_financed', '')
        c.drawRightString(19.9 * cm, (36.35 - 17.5) * cm, net_to_be_financed)

        charge_interest = data.validated_data.get('charge_interest', '')
        c.drawRightString(4.8 * cm, (35.8 - 17.5) * cm, charge_interest)

        charge_start_date = data.validated_data.get('charge_start_date', '')
        c.drawString(6.6 * cm, (35.8 - 17.5) * cm, charge_start_date)

        charge_due_date = data.validated_data.get('charge_due_date', '')
        c.drawString(9.3 * cm, (35.8 - 17.5) * cm, charge_due_date)

        charge_total_interest = data.validated_data.get('charge_total_interest', '')
        c.drawRightString(19.9 * cm, (35.75 - 17.5) * cm, charge_total_interest)

        if data.validated_data['charge_is_compound']:
            c.drawString(2.5 * cm, (35.3 - 17.5) * cm, "/")

        if data.validated_data['charge_is_simple']:
            c.drawString(2.5 * cm, (35.0 - 17.5) * cm, "/")

        if data.validated_data['charge_is_semi_monthly']:
            c.drawString(4.9 * cm, (35.3 - 17.5) * cm, "/")

        if data.validated_data['charge_is_weekly']:
            c.drawString(4.9 * cm, (35.0 - 17.5) * cm, "/")

        if data.validated_data['charge_is_quarterly']:
            c.drawString(7.7 * cm, (35.3 - 17.5) * cm, "/")

        if data.validated_data['charge_is_monthly']:
            c.drawString(7.7 * cm, (35.0 - 17.5) * cm, "/")

        if data.validated_data['charge_is_semi_annual']:
            c.drawString(9.9 * cm, (35.3 - 17.5) * cm, "/")

        if data.validated_data['charge_is_annual']:
            c.drawString(9.9 * cm, (35.0 - 17.5) * cm, "/")

        percentage_charge_to_total_net = data.validated_data.get('percentage_charge_to_total_net', '')
        c.drawRightString(12.55 * cm, (34.75 - 17.5) * cm, percentage_charge_to_total_net)

        effective_interest_rate = data.validated_data.get('effective_interest_rate', '')
        c.drawRightString(11 * cm, (33.1 - 17.5) * cm, effective_interest_rate)

        single_payment_due_date = data.validated_data.get('single_payment_due_date', None)
        if single_payment_due_date is not None:
            c.drawRightString(9.7 * cm, (31.35 - 17.5) * cm, single_payment_due_date.strftime('%B %d'))
            c.drawString(10.7 * cm, (31.35 - 17.5) * cm, single_payment_due_date.strftime('%y'))

        single_payment_amount = data.validated_data.get('single_payment_amount', '')
        c.drawRightString(19.9 * cm, (31.3 - 17.5) * cm, single_payment_amount)

        installment_monthly = data.validated_data['installment_monthly']

        init_y, delta_y = (30.4 - 17.5), 0.32
        for index, item in enumerate(installment_monthly):
            y = init_y - index * delta_y
            c.drawString(4.2 * cm, y * cm, item['term'])
            c.drawRightString(11 * cm, y * cm, item["amount_per_installment"])
            c.drawRightString(15.5 * cm, y * cm, item["total"])

        installment_total = data.validated_data.get('installment_total', '')
        c.drawRightString(19.75 * cm, (29.7 - 17.5) * cm, installment_total)

        pcci_tin = data.validated_data.get('pcci_tin', '')
        c.drawString(11.9 * cm, (26.6 - 17.5) * cm, pcci_tin)

        pcci_signatory = data.validated_data.get('pcci_signatory', '')
        c.drawString(11.4 * cm, (25.85 - 17.5) * cm, pcci_signatory.upper())

        pcci_signatory_position = data.validated_data.get('pcci_signatory_position', '')
        c.drawString(11.4 * cm, (24.85 - 17.5) * cm, pcci_signatory_position.upper())

        c.drawString(1.2 * cm, (24.9 - 17.5) * cm, borrower_name.upper())

        borrower_tin = data.validated_data.get('borrower_tin', '')
        c.drawString(1.7 * cm, (24.0 - 17.5) * cm, borrower_tin)

        if date is not None:
            c.drawString(2.0 * cm, (23.7 - 17.5) * cm, date.strftime('%B %d'))
            c.drawString(7.6 * cm, (23.7 - 17.5) * cm, date.strftime('%y'))

        c.setTitle(f'CM - Disclosure Statement')
        c.showPage()
        c.save()

        buffer.seek(0)
        return FileResponse(buffer, as_attachment=True, filename=f'CM - Disclosure Statement.pdf')
