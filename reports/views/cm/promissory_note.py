# -*- coding: utf-8 -*-
# @Time    : 1/28/21 9:28 PM
# @Author  : Red Periabras
# @Software: PyCharm

from io import BytesIO

from django.http import FileResponse
from reportlab.lib.pagesizes import letter
from reportlab.lib.units import cm
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen import canvas
from rest_framework import viewsets

from reports.serializers.cm.promissory_note import PromissoryNoteSerializer

import textwrap

FONT_SIZE = 7

pdfmetrics.registerFont(TTFont('F25_Bank_Printer', './reports/assets/fonts/F25_Bank_Printer.ttf'))

class CMPromissoryNoteView(viewsets.ModelViewSet):
    serializer_class = PromissoryNoteSerializer
    http_method_names = ['post']

    def create(self, request):
        data = self.serializer_class(data=request.data)

        if not data.is_valid(raise_exception=True):
            return

        buffer = BytesIO()
        c = canvas.Canvas(buffer, pagesize=letter)

        c.setFont('F25_Bank_Printer', size=FONT_SIZE)
        c.setStrokeColorRGB(169, 169, 169)

        date = data.validated_data['date']
        if date is not None:
            c.drawString(16.1 * cm, (65.7 - 42.7) * cm, date.strftime('%B %d'))
            c.drawString(19.1 * cm, (65.7 - 42.7) * cm, date.strftime('%y'))

        amount_in_figures = data.validated_data['amount_in_figures']
        c.drawString(1.5 * cm, (65.3 - 42.7) * cm, amount_in_figures)

        amount_in_words = data.validated_data['amount_in_words']
        c.drawString(1.2 * cm, (64.3 - 42.7) * cm, amount_in_words.upper())


        c.drawRightString(19.8 * cm, (64.3 - 42.7) * cm, amount_in_figures)

        loanterm_in_words = data.validated_data['loanterm_in_words']
        c.drawString(4.9 * cm, (63.95 - 42.7) * cm, loanterm_in_words.upper())

        loanterm_in_figures = data.validated_data['loanterm_in_figures']
        c.drawRightString(17.55 * cm, (63.95 - 42.7) * cm, str(loanterm_in_figures))

        monthlyamort_in_words = data.validated_data['monthlyamort_in_words']
        c.drawString(5 * cm, (63.6 - 42.7) * cm, monthlyamort_in_words.upper())

        monthyamort_in_figures = data.validated_data['monthlyamort_in_figures']
        c.drawRightString(4.1 * cm, (63.3 - 42.7) * cm,  monthyamort_in_figures)

        start_date = data.validated_data['start_date']
        if start_date is not None:
            c.drawString(7.3 * cm, (63.3 - 42.7) * cm, "%d%s" % (start_date.day,"tsnrhtdd"[(start_date.day//10%10!=1)*(start_date.day%10<4)*start_date.day%10::4]))
            c.drawString(11.8 * cm, (62.95 - 42.7) * cm, start_date.strftime("%B %d"))
            c.drawString(19.3 * cm, (62.95 - 42.7) * cm, start_date.strftime("%y"))

        maturity_date = data.validated_data['maturity_date']
        if maturity_date is not None:
            c.drawString(1.2 * cm, (62.95 - 42.7) * cm, maturity_date.strftime("%B %d"))
            c.drawString(7.2 * cm, (62.95 - 42.7) * cm, maturity_date.strftime("%y"))

        interest_in_words = data.validated_data['interest_in_words']
        c.drawString(7.2 * cm,  (62.6 - 42.7) * cm, interest_in_words)

        interest_in_figures = data.validated_data['interest_in_figures']
        c.drawRightString(19.6 * cm, (62.6 - 42.7) * cm, interest_in_figures)

        late_pay = data.validated_data['late_pay']
        c.drawRightString(12.35 * cm, (54.2 - 42.7) * cm, late_pay)

        proceeds_receiver = data.validated_data['proceeds_receiver']
        c.drawString(1.2 * cm, (50.6 - 42.7) * cm, proceeds_receiver.upper())

        loan_purpose = data.validated_data['loan_purpose']
        c.drawString(4.9 * cm, (47.55 - 42.7) * cm, loan_purpose)

        witness_1 = data.validated_data['witness_1']
        c.drawString(1.9 * cm, (46.8 - 42.7) * cm, witness_1.upper())

        witness_2 = data.validated_data['witness_2']
        c.drawString(1.9 * cm, (46.1 - 42.7) * cm, witness_2.upper())

        witness_3 = data.validated_data['witness_3']
        c.drawString(13.2 * cm, (46.8 - 42.7) * cm, witness_3.upper())

        witness_4 = data.validated_data['witness_4']
        c.drawString(13.2 * cm, (46.1 - 42.7) * cm, witness_4.upper())

        c.setTitle(f'CM - Promissory Note')
        c.showPage()
        c.save()

        buffer.seek(0)
        return FileResponse(buffer, as_attachment=True, filename=f'CM - Promissory Note.pdf')
