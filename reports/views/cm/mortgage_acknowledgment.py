# -*- coding: utf-8 -*-
# @Time    : 2/5/21 10:06 PM
# @Author  : Red Periabras
# @File    : mortgage_acknowledgment.py
# @Software: PyCharm


from io import BytesIO

from django.http import FileResponse
from reportlab.lib.pagesizes import letter
from reportlab.lib.units import cm
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen import canvas
from rest_framework import viewsets

from reports.serializers.cm.mortgage_acknowledgment import MortgageAcknowledgmentSerializer

FONT_SIZE = 7

pdfmetrics.registerFont(TTFont('F25_Bank_Printer', './reports/assets/fonts/F25_Bank_Printer.ttf'))


class MortgageAcknowledgmentView(viewsets.ModelViewSet):
    serializer_class = MortgageAcknowledgmentSerializer
    http_method_names = ['post']

    def create(self, request):
        data = self.serializer_class(data=request.data)

        if not data.is_valid(raise_exception=True):
            return

        buffer = BytesIO()
        c = canvas.Canvas(buffer, pagesize=letter)

        c.setFont('F25_Bank_Printer', size=FONT_SIZE)
        c.setStrokeColorRGB(169, 169, 169)

        borrower_name = data.validated_data['borrower_name']
        c.drawString(2.3 * cm, (20.9 + 5.25) * cm, borrower_name.upper())

        mortgagee_name = data.validated_data['mortgagee_name']
        c.drawString(12.5 * cm, (20.9 + 5.25) * cm, mortgagee_name.upper())

        reserve_field = data.validated_data['reserve_field']
        c.drawString(1.43 * cm, (19.3 + 5.25) * cm, reserve_field)

        date = data.validated_data['date']
        if date is not None:
            c.drawString(16.2 * cm, (17.7 + 5.25) * cm, "%d%s" % (date.day,"tsnrhtdd"[(date.day//10%10!=1)*(date.day%10<4)*date.day%10::4]))
            c.drawString(17.75 * cm, (17.7 + 5.25) * cm, date.strftime('%B'))
            c.drawString(20.1 * cm, (17.7 + 5.25) * cm, date.strftime('%y'))

        notary_city = data.validated_data['notary_city']
        c.drawString(7.6 * cm, (17.7 + 5.25) * cm, notary_city)

        pcci_id = data.validated_data['pcci_id']
        c.drawString(7.75 * cm, (15.95 + 5.25) * cm, pcci_id['government_id'])
        c.drawString(12.6 * cm, (15.95 + 5.25) * cm, f'{pcci_id["expiry_date"]}/{pcci_id["place_of_issue"]}')

        init_y, delta_y = (15.3 + 5.5), 0.45
        presented_ids = data.validated_data['presented_ids']
        for index, item in enumerate(presented_ids):
            y = init_y - index * delta_y
            c.drawString(3.0 * cm, y * cm, item['name'])
            c.drawString(7.75 * cm, y * cm, item['government_id'])
            c.drawString(12.6 * cm, y * cm, f'{item["expiry_date"]}/{item["place_of_issue"]}')

        c.drawString(2.5 * cm, (6.7 + 5.5) * cm, borrower_name.upper())

        borrower_address = data.validated_data['borrower_address']
        c.drawString(12.3 * cm, (6.7 + 5.5) * cm, borrower_address.upper())

        c.drawString(12.5 * cm, (4.3 + 5.5) * cm, borrower_name.upper())

        c.setTitle('CM - Mortgage Acknowledgment')
        c.showPage()
        c.save()

        buffer.seek(0)
        return FileResponse(buffer, as_attachment=True, filename=f'CM - Mortgage Acknowledgment.pdf')
