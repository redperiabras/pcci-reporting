# -*- coding: utf-8 -*-
# @Time    : 1/27/21 9:07 PM
# @Author  : Red Periabras
# @Email   : redperiabras@gmail.com
# @File    : urls.py
# @Software: PyCharm

from django.urls import path

from .views.official_receipt import OfficialReceiptView
from .views.plp.promissory_note import PLPPromissoryNoteView
from .views.plp.disclosure_statement import PLPDisclosureStatementView
from .views.plp.authority_to_deduct import PLPAuthorityToDeductView
from .views.cm.promissory_note import CMPromissoryNoteView
from .views.cm.disclosure_statement import CMDisclosureStatmentView
from .views.cm.mortgage_declaration import MortgageDeclarationView
from .views.cm.mortgage_witnesses import MortgageWitnessesView
from .views.cm.mortgage_acknowledgment import MortgageAcknowledgmentView
from .views.rp.promissory_note import RPPromissoryNoteView
from .views.rp.disclosure_statement import RPDisclosureStatementView
from .views.rp.acknowledgment import RPAcknowledgment
from .views.rp.deed_of_assignment import RPDeedOfAssignment
from .views.rem.promissory_note import REMPromissoryNoteView
from .views.rem.disclosure_statement import REMDisclosureStatementView
from .views.rem.mortgage_declaration import REMMortgageDeclarationView
from .views.rem.mortgage_acknowledgment import REMMortgageAcknowledgmentView

urlpatterns = [
    path("or/", OfficialReceiptView.as_view({'post':'create'}), name='or'),
    path("plp/promissory-note/", PLPPromissoryNoteView.as_view({'post': 'create'}), name='plp-promissory-note'),
    path("plp/disclosure-statement/", PLPDisclosureStatementView.as_view({'post': 'create'}), name='plp-disclosure-statement'),
    path("plp/authority-to-deduct/", PLPAuthorityToDeductView.as_view({'post': 'create'}), name='plp-authority-to-deduct'),

    path("cm/promissory-note/", CMPromissoryNoteView.as_view({'post': 'create'}), name='cm-promissory-note'),
    path("cm/disclosure-statement/", CMDisclosureStatmentView.as_view({'post': 'create'}), name='cm-disclosure-stateement'),
    path("cm/mortgage-declaration/", MortgageDeclarationView.as_view({'post': 'create'}), name='cm-mortgage-declaration'),
    path("cm/mortgage-witnesses/", MortgageWitnessesView.as_view({'post': 'create'}), name='cm-mortgage-witnesses'),
    path("cm/mortgage-acknowledgment/", MortgageAcknowledgmentView.as_view({'post': 'create'}), name='cm-mortgage-witnesses'),

    path("rp/promissory-note/", RPPromissoryNoteView.as_view({'post': 'create'}), name='rp-promissory-note'),
    path("rp/disclosure-statement/", RPDisclosureStatementView.as_view({'post': 'create'}), name='rp-disclosure-statement'),
    path("rp/acknowledgment/", RPAcknowledgment.as_view({'post': 'create'}), name='rp-acknowledgment'),
    path('rp/deed-of-assignment', RPDeedOfAssignment.as_view({'post': 'create'}), name='rp-deed-of-assignment'),

    path("rem/promissory-note/", REMPromissoryNoteView.as_view({'post': 'create'}), name='rem-promissory-note'),
    path("rem/disclosure-statement/", REMDisclosureStatementView.as_view({'post': 'create'}), name='rem-disclosure-statement'),
    path("rem/mortgage-declaration/", REMMortgageDeclarationView.as_view({'post': 'create'}), name='rem-mortgage-declaration'),
    path("rem/mortgage-acknowledgment/", REMMortgageAcknowledgmentView.as_view({'post': 'create'}), name='rem-mortgage-acknowledgment')
]