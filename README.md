# PCCI Reporting
A django-based api that generates a document to overlay existing documents of PCCI

## Prerequisites
* [Python 3](https://www.python.org/downloads/)
* [pipenv](http://www.sis.pitt.edu/mbsclass/tutorial/advanced/makefile/whatis.htm)
* [make](http://www.sis.pitt.edu/mbsclass/tutorial/advanced/makefile/whatis.htm)

## Environment Setup
To initialize the python environment, execute the following command:

```shell
make build
```

## How to run?
To run the project just execute the following comand:

```sh
make runserver
```

## API Documentation
Documentation is available upon running the API using the following endpoint:
```shell
http://<host:8000>/docs
```

## Ready to Integrate
* [Official Receipt]()
* [PLP Promissory Note]()
* [PLP Disclosure Statement]()
* [PLP Authority to Deduct]()

## TODO
* [CM PAGE 1 (Promissory Note, Disclosure Statement, etc)]()
* [CM PAGE 2 (Acknowledgment, Affidavit of Good Faith)]()
* [REM Promissory Note]()
* [REM Disclosure Statement]()
* [REM PAGE 1]()
* [REM PAGE 1 (Acknowledgment)]()
* [RP Promissory Note]()
* [RP Disclosure Statment]()
* [RP Deed of Assigment]()
* [RP Acknowledgement]()
 