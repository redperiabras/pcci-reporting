SHELL := /bin/bash

.PHONY:
	clean
	build
	run
	test

clean:
	find . -iname "*.pyc" | xargs rm -Rf
	find . -iname "*.pyo" | xargs rm -Rf
	find . -iname "*.pyd" | xargs rm -Rf
	find . -iname "__pycache__" | xargs rm -Rf

build:
	make clean
	export PIPENV_VENV_IN_PROJECT=1
	pip install pipenv --upgrade
	pipenv install
	pipenv run python manage.py collectstatic --no-input
	pipenv run ./manage.py migrate

runserver:
	pipenv run gunicorn pcci_reporting.wsgi --bind 0.0.0.0 --workers 4 --threads 4 --reload --access-logfile ./logs/gunicorn-access.log --error-logfile ./logs/gunicorn-error.log --log-level debug --worker-class gevent